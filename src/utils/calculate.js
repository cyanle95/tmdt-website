export const CalculateSalePrice = (price, percent) => {
  return Math.round(((price / 100) * (100 - percent) + Number.EPSILON) * 100) / 100;
};

export const CalculatePrice = (price) => {
  return price;
};

export const num2numDong = (num, currency, currencyLabel) => {
  /**
   * Tự động phân cách 3 số sau dấu chấm
   */
  const positiveNum = Math.abs(num);
  let numString = '';

  const cumulativeRevenue = Array.from(parseFloat(positiveNum).toFixed(0)).reverse();
  let beautyCumulativeRevenue = '';
  cumulativeRevenue.forEach((number, index) => {
    beautyCumulativeRevenue += number;

    if (index && !((index + 1) % 3)) {
      beautyCumulativeRevenue += '.';
    }
  });
  beautyCumulativeRevenue = beautyCumulativeRevenue.replace(/[.]$/, '');
  numString = Array.from(beautyCumulativeRevenue).reverse().join('');
  if (currency !== false) {
    numString += currencyLabel ? ` ${currencyLabel}` : ' đ';
  }
  if (num !== 0 && num < 0) {
    return `-${numString}`;
  }
  return numString;
};

export const kFormatter = (num) => {
  return Math.abs(num) > 999999
    ? `${Math.sign(num) * (Math.abs(num) / 1000000).toFixed(1)}tr`
    : Math.abs(num) > 999
    ? `${Math.sign(num) * (Math.abs(num) / 1000).toFixed(1)}k`
    : Math.sign(num) * Math.abs(num);
};
