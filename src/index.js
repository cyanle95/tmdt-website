import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import App from "./App";
import { combinedReducer, rootSaga } from "./store";
import "./assets/css/main.css";
import "react-responsive-carousel/lib/styles/carousel.min.css";

const sagaMiddleware = createSagaMiddleware();
const store = createStore(combinedReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

// Since we are using HtmlWebpackPlugin WITHOUT a template,
// we create our own root node in the body element before rendering into it
let root = document.createElement("div");
root.id = "root";

document.body.appendChild(root);
// Now we can render our application into it
render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  root
);
