import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { LocalizeProvider, withLocalize } from "react-localize-redux";
import { getCategories } from "./store/category/actions";
import initializeLanguage from "./translations";
import Layout from "./containers/Common/Layout";
import Home from "./containers/Home";

const App = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCategories());
  });

  return (
    <LocalizeProvider initialize={initializeLanguage}>
      <BrowserRouter>
        <Layout>
          <Switch>
            <Route exact path="/" component={Home} />
          </Switch>
        </Layout>
      </BrowserRouter>
    </LocalizeProvider>
  );
};

export default withLocalize(App);
