import globalTranslations from './global';
import ReactDOMServer from 'react-dom/server';

const initializeLanguage = {
  languages: [
    { name: 'Vietnam', code: 'vi' },
    { name: 'English', code: 'en' }
  ],
  translation: globalTranslations,
  options: {
    renderToStaticMarkup: ReactDOMServer.renderToStaticMarkup,
    defaultLanguage: 'vi'
  }
};

export default initializeLanguage;