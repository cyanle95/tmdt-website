import React from "react";

import { ReactComponent as CartIcon } from "../../assets/images/icons/shopping.svg";
import { ReactComponent as HeartIcon } from "../../assets/images/icons/heart.svg";
import { ReactComponent as EyeOpenIcon } from "../../assets/images/icons/eye-open.svg";
import { ReactComponent as ResizeIcon } from "../../assets/images/icons/resize.svg";
import { ReactComponent as StarIcon } from "../../assets/images/icons/star.svg";
import { ReactComponent as MenuIcon } from "../../assets/images/icons/menu.svg";

const Icon = (props) => {
  switch (props.name) {
    case "cart":
      return <CartIcon {...props} />;
    case "eye-open":
      return <EyeOpenIcon {...props} />;
    case "heart":
      return <HeartIcon {...props} />;
    case "resize":
      return <ResizeIcon {...props} />;
    case "star":
      return <StarIcon {...props} />;
    case "menu":
      return <MenuIcon {...props} />;
    default:
      return <div />;
  }
};

export default Icon;
