import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setOpenSidebar } from "../../store/ui/actions";
import Icon from "../Icon";
import "./style.css";

const Menu = () => {
  const { categories } = useSelector((state) => state.categoryManagement);
  const dispatch = useDispatch();
  const [isShowMenu, setIsShowMenu] = useState(false);
  const { isOpenSidebar } = useSelector((state) => state.ui);
  const setIsShowOpenSidebar = () => {
    dispatch(setOpenSidebar(!isOpenSidebar));
  };
  console.log(isOpenSidebar);
  return (
    <div className="w-full">
      <div class={`lx:hidden ${isOpenSidebar ? "" : "hidden"}`}>
        <div class="fixed inset-0 flex z-40">
          <div class="fixed inset-0" aria-hidden="true">
            <div class="absolute inset-0 bg-gray-600 opacity-75"></div>
          </div>

          <div class="relative flex-1 flex flex-col max-w-xs w-full pt-5 pb-4 bg-white">
            <div class="absolute top-0 right-0 -mr-12 pt-2">
              <button
                class="ml-1 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                onClick={setIsShowOpenSidebar}
              >
                <span class="sr-only">Close sidebar</span>

                <svg
                  class="h-6 w-6 text-white"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  aria-hidden="true"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M6 18L18 6M6 6l12 12"
                  />
                </svg>
              </button>
            </div>
            <div class="mt-5 flex-1 h-0 overflow-y-auto">
              {categories.map((c, key) => {
                return (
                  <div
                    className={`w-full h-10 py-3 px-5 hover:bg-gray-100 flex justify-start items-center ${
                      key === 0 ? "mt-3" : ""
                    }`}
                    key={key}
                  >
                    {c.name}
                  </div>
                );
              })}
            </div>
          </div>
          <div class="flex-shrink-0 w-14" aria-hidden="true"></div>
        </div>
      </div>
      <div className="hidden xl:block w-full bg-gray-100">
        <div className="max-w-1200 py-2 mx-auto flex justify-start items-center">
          <div
            className={`w-60 h-10 px-5 bg-black rounded-t-md ${
              isShowMenu ? "" : "rounded-b-md"
            } cursor-pointer text-white flex justify-start items-center relative`}
            onClick={() => {
              setIsShowMenu(!isShowMenu);
            }}
          >
            <div className="w-1/3 px-5 flex justify-end">
              <Icon name="menu" className="h-4 w-4 fill-current text-white" />
            </div>
            <span className="font-medium">Shop By Category</span>
            <div
              className={`menu-content w-60 bg-white text-black font-medium rounded-b-md absolute z-10 top-10 left-0 border-l-2 border-r-2 overflow-hidden border-black transition-all duration-1000 ${
                isShowMenu ? "h-56 border-b-2 border-t-2" : "h-0 "
              }`}
            >
              {categories.map((c, key) => {
                return (
                  <div
                    className={`w-full h-10 py-3 px-5 hover:bg-gray-100 flex justify-start items-center ${
                      key === 0 ? "mt-3" : ""
                    }`}
                    key={key}
                  >
                    {c.name}
                  </div>
                );
              })}
            </div>
          </div>
          <div className="flex justify-start items-center">
            {categories.map((c, key) => {
              return (
                <a className="text-black font-medium px-8" key={key}>
                  {c.name}
                </a>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Menu;
