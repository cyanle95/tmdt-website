import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import UserMenu from "./UserMenu";
import Icon from "../../components/Icon";
import logo from "../../assets/images/logo.png";
import "./styles.css";
import { setOpenSidebar } from "../../store/ui/actions";

const TopBar = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { isOpenSidebar } = useSelector((state) => state.ui);
  const setIsShowOpenSidebar = () => {
    dispatch(setOpenSidebar(!isOpenSidebar));
  };
  return (
    <div className="w-full">
      {/* start hidden in tablet */}
      <div className="hidden xl:block w-full h-11 bg-black">
        <div className="max-w-1200 h-full m-auto text-white flex justify-between items-center">
          <span>Get Special 27% Discount On Electronic Items</span>
          <div>
            <span className="pr-10">English</span>
            <span>USD</span>
          </div>
        </div>
      </div>
      <div className="hidden xl:block w-full bg-white">
        <div className="max-w-1200 h-full m-auto text-white flex justify-between items-center py-8">
          <div className="w-1/4 flex justify-start items-center">
            <img
              className="h-10 w-auto"
              src={logo}
              onClick={() => history.push("/")}
            />
          </div>
          <div className="w-2/4">
            <div className="flex">
              <input
                className="h-10 w-550 border-t-2 border-l-2 border-b-2 border-gray-200 text-black px-5 rounded-tl-md rounded-bl-md focus:outline-none"
                placeholder="Search Product Here..."
              />
              <button className="h-10 w-24 bg-black rounded-tr-md rounded-br-md">
                SEARCH
              </button>
            </div>
          </div>
          <div className="w-1/4 flex justify-end items-center">
            <Icon name="cart" className="h-6 w-6" />
            <div className="relative w-4 h-4 rounded-full font-bold  text-white top-minus-10 left-minus-5 bg-red-700 flex justify-center items-center">
              3
            </div>
            <UserMenu />
          </div>
        </div>
      </div>
      {/* end hidden in tablet */}

      {/* start show in tablet */}
      <div className="xl:hidden w-full h-16 bg-white text-black flex justify-center items-center">
        <div className="w-1/3 px-2 lg:px- flex justify-start">
          <Icon
            name="menu"
            className="h-6 w-6"
            onClick={setIsShowOpenSidebar}
          />
        </div>
        <div className="w-1/3 px-2 lg:px- flex justify-center">
          <img
            className="h-6 lg:h-10 w-auto"
            src={logo}
            onClick={() => history.push("/")}
          />
        </div>
        <div className="w-1/3 px-2 lg:px-5 flex justify-end items-center">
          <Icon name="cart" className="h-6 w-6" />
          <div className="relative w-4 h-4 rounded-full font-bold  text-white top-minus-10 left-minus-5 bg-red-700 flex justify-center items-center">
            3
          </div>
          <UserMenu />
        </div>
      </div>
      {/* end show in tablet */}
    </div>
  );
};

export default TopBar;
