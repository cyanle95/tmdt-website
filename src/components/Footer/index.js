import React from "react";

const Footer = (props) => {
  return (
    <div className="w-full bg-black">
      <div className="max-w-1200 m-auto text-center">
        <div className="h-12 text-gray-200 flex justify-center items-center">
          <span>Copyright © 2020 DaoTV@PI.COM.VN</span>
        </div>
      </div>
    </div>
  );
};

export default Footer;
