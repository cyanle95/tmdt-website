import { UpCircleFilled } from "@ant-design/icons";
import API from "../../constants/api";
import configuredAxios from "../../utils/configured-axios";

const imageServices = {
  delete: (id) => {
    const results = configuredAxios.delete(API.IMAGE.delete(id));
    return results;
  },
};
export default imageServices;
