import { UNIT_ACTIONS } from "./actionTypes";

export const getUnits = (params) => {
  return {
    type: UNIT_ACTIONS.GET_UNITS,
    params,
  };
};

export const getUnitsSuccess = (units) => {
  return {
    type: UNIT_ACTIONS.GET_UNITS_SUCCESS,
    units,
  };
};
