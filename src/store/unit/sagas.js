import { put, takeEvery, call } from "redux-saga/effects";
import unitService from "./services";
import { UNIT_ACTIONS } from "./actionTypes";
import { getUnitsSuccess } from "./actions";

function* getUnitsSaga(action) {
  try {
    const response = yield call(() => unitService.getUnits(action.params));
    let { units } = response.data.data;
    // convert data select option
    let data = [];
    units.map((item) => {
      let itemNew = {};
      itemNew.value = item.unitId;
      itemNew.name = item.unitName;
      data.push(itemNew);
    });
    yield put(getUnitsSuccess(data));
  } catch (error) {
    // yield put(loginFailure());
  }
}

function* watchAll() {
  yield takeEvery(UNIT_ACTIONS.GET_UNITS, getUnitsSaga);
}

export default watchAll();
