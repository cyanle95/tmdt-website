import API from "../../constants/api";
import configuredAxios from "../../utils/configured-axios";

const unitServices = {
  getUnits: (params) => {
    const results = configuredAxios.get(API.UNIT.GET_LIST, params);
    return results;
  },
};
export default unitServices;
