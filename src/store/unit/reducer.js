import { UNIT_ACTIONS } from "./actionTypes";
const initialState = {
  isLoading: false,
  units: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case UNIT_ACTIONS.GET_UNITS:
      return { ...state, isLoading: true };
    case UNIT_ACTIONS.GET_UNITS_SUCCESS:
      return { ...state, isLoading: false, units: action.units };
    case UNIT_ACTIONS.GET_UNITS_FAILURE:
      return { ...state, isLoading: false };
    default:
      return state;
  }
};

export default reducer;
