import { put, takeEvery, call } from "redux-saga/effects";
import shoppingService from "./services";
import { SHOPPING_ACTIONS } from "./actionTypes";
import { getProductsSuccess, getListBannerSuccess, getProductsByCategorySuccess } from "./actions";

function* getProductSaga(action) {
  try {
    const response = yield call(() => shoppingService.getListProduct());
    console.log('saga - product - res:', response);
    const {products} = response?.data?.data;
    yield put(getProductsSuccess(products));
  } catch (error) {
    console.log('saga - failed:', error);
  }
}
function* getProductByCategorySaga(action) {
  console.log('xxxxx');
  
  try {
    const response = yield call(() => shoppingService.getListProductByCategory(action.categoryId));
    console.log('saga - productbyCategory - res111111:', response);
    const {products} = response?.data?.data;
    yield put(getProductsByCategorySuccess(products));
  } catch (error) {
    console.log('saga - failed:', error);
  }
}
function* getBannerSaga(action) {
  try {
    const response = yield call(() => shoppingService.getListBanner());
    console.log('saga - banner - res:', response);
    const {banners} = response?.data?.data;
    yield put(getListBannerSuccess(banners));
  } catch (error) {
    console.log('saga - failed:', error);
  }
}

function* watchAll() {
  yield takeEvery(SHOPPING_ACTIONS.GET_PRODUCTS, getProductSaga);
  yield takeEvery(SHOPPING_ACTIONS.GET_BANNERS, getBannerSaga);
  yield takeEvery(SHOPPING_ACTIONS.GET_PRODUCTS_BY_CATEGORY, getProductByCategorySaga);
}

export default watchAll();
