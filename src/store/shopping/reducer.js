import { SHOPPING_ACTIONS } from "./actionTypes";
const initialState = {
  isLoading: false,
  products: [],
  productsByCategory: [],
  banners: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOPPING_ACTIONS.GET_PRODUCTS:
      return { ...state, isLoading: true };
    case SHOPPING_ACTIONS.GET_PRODUCTS_SUCCESS:
      return { ...state, isLoading: false, products: action.products };
    case SHOPPING_ACTIONS.GET_PRODUCTS_FAILED:
      return { ...state, isLoading: false };
    case SHOPPING_ACTIONS.GET_PRODUCTS_BY_CATEGORY:
      return { ...state, isLoading: true };
    case SHOPPING_ACTIONS.GET_PRODUCTS_BY_CATEGORY_SUCCESS:
      return { ...state, isLoading: false, productsByCategory: action.products };
    case SHOPPING_ACTIONS.GET_PRODUCTS_BY_CATEGORY_FAILED:
      return { ...state, isLoading: false };
    case SHOPPING_ACTIONS.GET_BANNERS:
      return { ...state, isLoading: true };
    case SHOPPING_ACTIONS.GET_BANNERS_SUCCESS:
      return { ...state, isLoading: false, banners: action.banners };
    case SHOPPING_ACTIONS.GET_BANNERS_FAILED:
      return { ...state, isLoading: false };
    default:
      return state;
  }
};

export default reducer;
