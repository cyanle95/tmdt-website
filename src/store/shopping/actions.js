import { SHOPPING_ACTIONS } from "./actionTypes";

export const getProducts = () => {
  return {
    type: SHOPPING_ACTIONS.GET_PRODUCTS,
  };
};

export const getProductsSuccess = (products) => {
  return {
    type: SHOPPING_ACTIONS.GET_PRODUCTS_SUCCESS,
    products,
  };
};

export const getProductsFailed = () => {
  return {
    type: SHOPPING_ACTIONS.GET_PRODUCTS_FAILED,
  };
};
export const getProductsByCategory = (categoryId) => {
  return {
    type: SHOPPING_ACTIONS.GET_PRODUCTS_BY_CATEGORY,
    categoryId
  };
};

export const getProductsByCategorySuccess = (products) => {
  return {
    type: SHOPPING_ACTIONS.GET_PRODUCTS_BY_CATEGORY_SUCCESS,
    products,
  };
};

export const getProductsByCategoryFailed = () => {
  return {
    type: SHOPPING_ACTIONS.GET_PRODUCTS_BY_CATEGORY_FAILED,
  };
};
export const getListBanner = () => {
  return {
    type: SHOPPING_ACTIONS.GET_BANNERS,
  };
};

export const getListBannerSuccess = (banners) => {
  return {
    type: SHOPPING_ACTIONS.GET_BANNERS_SUCCESS,
    banners,
  };
};

export const getListBannerFailed = () => {
  return {
    type: SHOPPING_ACTIONS.GET_BANNERS_FAILED,
  };
};