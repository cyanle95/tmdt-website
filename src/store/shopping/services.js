import API from "../../constants/api";
import configuredAxios from "../../utils/configured-axios";

const shoppingService = {
  getListProduct: () => {
    const results = configuredAxios.get(API.PRODUCT.GET_LIST);
    return results;
  },//GET_LIST_BY_CATEGORY
  getListProductByCategory: (categoryId) => {
    const url = API.PRODUCT.GET_LIST;
    const params = {categoryId: `'${categoryId}'`};
    console.log('URL:', url);
    
    const results = configuredAxios.get(url, params);
    return results;
  },
  getListBanner: () => {
    const results = configuredAxios.get(API.BANNER.GET_LIST);
    return results;
  },
};
export default shoppingService;
