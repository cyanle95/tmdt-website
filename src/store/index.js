import { combineReducers } from "redux";
import { all } from "redux-saga/effects";

// Import all reducers here
import uiReducer from "./ui/reducer";
import notificationReducer from "./notification/reducer";
import categoryReducer from "./category/reducer";
import shoppingReducer from "./shopping/reducer";
import categorySagas from "./category/sagas";
import shoppingSagas from "./shopping/sagas";

export const combinedReducer = combineReducers({
  ui: uiReducer,
  notification: notificationReducer,
  categoryManagement: categoryReducer,
  shoppingManagement: shoppingReducer,
});

export const rootSaga = function* () {
  yield all([
    ...categorySagas,
    ...shoppingSagas,
  ]);
};
