import { UI_ACTIONS } from "./actionTypes";

const { IS_OPEN_SIDEBAR } = UI_ACTIONS;

export const setOpenSidebar = (payload) => ({
  type: IS_OPEN_SIDEBAR,
  payload,
});
