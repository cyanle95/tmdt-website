import { UI_ACTIONS } from "./actionTypes";

const { IS_OPEN_SIDEBAR } = UI_ACTIONS;

const initialState = {
  isOpenSidebar: false,
};

const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case IS_OPEN_SIDEBAR:
      return {
        ...state,
        isOpenSidebar: payload,
      };
    default:
      return state;
  }
};

export default reducer;
