import { put, takeEvery, call } from "redux-saga/effects";
import categoryService from "./services";
import { CATEGORY_ACTIONS } from "./actionTypes";
import { getCategoriesSuccess } from "./actions";

function* getCategoriesSaga(action) {
  try {
    const response = yield call(() => categoryService.getCategories());
    console.log('saga - res:', response);
    
    let { categories } = response.data.data;
    let data = [];
    categories.map((item) => {
      let itemNew = {};
      itemNew.value = item.categoryId;
      itemNew.name = item.categoryName;
      data.push(itemNew);
    });
    yield put(getCategoriesSuccess(data));
  } catch (error) {
    console.log('saga - failed:', error);
    
    // yield put(loginFailure());
  }
}

function* watchAll() {
  yield takeEvery(CATEGORY_ACTIONS.GET_CATEGORIES, getCategoriesSaga);
}

export default watchAll();
