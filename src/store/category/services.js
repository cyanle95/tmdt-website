import API from "../../constants/api";
import configuredAxios from "../../utils/configured-axios";

const categoryServices = {
  getCategories: () => {
    const results = configuredAxios.get(API.CATEGORY.GET_LIST);
    return results;
  },
  getTopics: (categoryId) => {
    const results = configuredAxios.get(`${API.CATEGORY.GET_LIST_TOPICS}/${categoryId}/topics`);
    return results;
  },
};
export default categoryServices;
