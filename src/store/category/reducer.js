import { CATEGORY_ACTIONS } from "./actionTypes";
const initialState = {
  isLoading: false,
  categories: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CATEGORY_ACTIONS.GET_CATEGORIES:
      return { ...state, isLoading: true };
    case CATEGORY_ACTIONS.GET_CATEGORIES_SUCCESS:
      return { ...state, isLoading: false, categories: action.categories };
    case CATEGORY_ACTIONS.GET_CATEGORIES_FAILED:
      return { ...state, isLoading: false };
    default:
      return state;
  }
};

export default reducer;
