import { CATEGORY_ACTIONS } from "./actionTypes";

export const getCategories = () => {
  return {
    type: CATEGORY_ACTIONS.GET_CATEGORIES,
  };
};

export const getCategoriesSuccess = (categories) => {
  return {
    type: CATEGORY_ACTIONS.GET_CATEGORIES_SUCCESS,
    categories,
  };
};

export const getCategoriesFailed = () => {
  return {
    type: CATEGORY_ACTIONS.GET_CATEGORIES_FAILED,
  };
};