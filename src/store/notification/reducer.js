import { NOTIFICATION_ACTIONS } from "./actionTypes";
const initialState = {
  isShow: false,
  notificationType: "",
  notificationMessage: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case NOTIFICATION_ACTIONS.SHOW_NOTIFICATION:
      return {
        ...state,
        isShow: true,
        notificationType: action.notificationType,
        notificationMessage: action.notificationMessage,
      };
    case NOTIFICATION_ACTIONS.HIDDEN_NOTIFICATION:
      return {
        ...state,
        isShow: false,
        notificationMessage: "",
      };
    default:
      return state;
  }
};

export default reducer;
