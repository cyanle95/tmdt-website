import { NOTIFICATION_ACTIONS } from "./actionTypes";

export const showNotification = (notificationType, notificationMessage) => {
  return {
    type: NOTIFICATION_ACTIONS.SHOW_NOTIFICATION,
    notificationType,
    notificationMessage,
  };
};

export const hiddenNotification = () => {
  return {
    type: NOTIFICATION_ACTIONS.HIDDEN_NOTIFICATION,
  };
};
