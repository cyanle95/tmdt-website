export const orders = [
  {
    id: 1,
    orderNo: "ABCD",
    statusId: 1,
    reason: "NO reason",
    note: "NO note",
    addressId: "Nam tu liem",
  },
  {
    id: 2,
    orderNo: "EFGH",
    statusId: 1,
    reason: "NO reason",
    note: "NO note",
    addressId: "Nam tu liem",
  },
  {
    id: 3,
    orderNo: "IJKL",
    statusId: 1,
    reason: "NO reason",
    note: "NO note",
    addressId: "Nam tu liem",
  },
];

export const tableColumns = [
  {
    title: "Order number",
    key: "orderNo",
    dataIndex: "orderNo",
  },
  {
    title: "Status",
    key: "statusId",
    dataIndex: "statusId",
  },
  {
    title: "Reason",
    key: "reason",
    dataIndex: "reason",
  },
  {
    title: "Note",
    key: "note",
    dataIndex: "note",
  },
  {
    title: "Address",
    key: "address",
    dataIndex: "address",
  },
];
