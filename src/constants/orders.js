import React from "react";

export const ORDER_STATUS = {
  0: "Tất cả",
  3: "Chờ xác nhận",
  5: "Đã huỷ",
  7: "Đã xác nhận",
  11: "Đang đợi giao",
  13: "Đã nhận hàng",
  17: "Hoàn thành",
};

export const NO_SELECTION_STATUSES = [0, 5, 11, 17];

export const PAGE_BUTTON = {
  0: [],
  3: [
    {
      text: "Xác nhận",
      nextState: 7,
      className: "bg-green-500 text-white",
    },
    {
      text: "Huỷ",
      nextState: 5,
      className: "bg-red-500 text-white",
    },
  ],
  5: [],
  7: [
    {
      text: "Giao tài xế",
      nextState: 11,
      className: "bg-green-500 text-white",
    },
  ],
  11: [],
  13: [
    {
      text: "Đã xác nhận",
      nextState: 17,
      className: "bg-green-500 text-white",
    },
  ],
  17: [],
};

export const ORDER_COLUMNS = {
  0: [
    {
      title: "Order number",
      key: "orderNumber",
      dataIndex: "orderNumber",
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      render: (status) => ORDER_STATUS[status],
    },
    {
      title: "User",
      key: "userName",
      dataIndex: "userName",
    },
    {
      title: "Phone",
      key: "userPhone",
      dataIndex: "userPhone",
    },
    {
      title: "Payment Method",
      key: "paymentName",
      dataIndex: "paymentName",
    },
    {
      title: "Price",
      key: "orderPrice",
      dataIndex: "orderPrice",
    },
    {
      title: "Shipping Fees",
      key: "shippingFee",
      dataIndex: "shippingFee",
    },
  ],
  3: [
    {
      title: "Order number",
      key: "orderNumber",
      dataIndex: "orderNumber",
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      render: (status) => ORDER_STATUS[status],
    },
    {
      title: "User",
      key: "userName",
      dataIndex: "userName",
    },
    {
      title: "Phone",
      key: "userPhone",
      dataIndex: "userPhone",
    },
    {
      title: "Payment Method",
      key: "paymentName",
      dataIndex: "paymentName",
    },
    {
      title: "Price",
      key: "orderPrice",
      dataIndex: "orderPrice",
    },
    {
      title: "Shipping Fees",
      key: "shippingFee",
      dataIndex: "shippingFee",
    },
  ],
  5: [
    {
      title: "Order number",
      key: "orderNumber",
      dataIndex: "orderNumber",
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      render: (status) => ORDER_STATUS[status],
    },
    {
      title: "Reason",
      key: "reason",
      dataIndex: "reason",
    },
    {
      title: "User",
      key: "userName",
      dataIndex: "userName",
    },
    {
      title: "Phone",
      key: "userPhone",
      dataIndex: "userPhone",
    },
    {
      title: "Payment Method",
      key: "paymentName",
      dataIndex: "paymentName",
    },
    {
      title: "Price",
      key: "orderPrice",
      dataIndex: "orderPrice",
    },
    {
      title: "Shipping Fees",
      key: "shippingFee",
      dataIndex: "shippingFee",
    },
  ],
  7: [
    {
      title: "Order number",
      key: "orderNumber",
      dataIndex: "orderNumber",
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      render: (status) => ORDER_STATUS[status],
    },
    {
      title: "User",
      key: "userName",
      dataIndex: "userName",
    },
    {
      title: "Phone",
      key: "userPhone",
      dataIndex: "userPhone",
    },
    {
      title: "Payment Method",
      key: "paymentName",
      dataIndex: "paymentName",
    },
    {
      title: "Price",
      key: "orderPrice",
      dataIndex: "orderPrice",
    },
    {
      title: "Shipping Fees",
      key: "shippingFee",
      dataIndex: "shippingFee",
    },
    {
      title: "Select Driver",
      render: () => (
        <select value="driverId">
          <option value="1">Nguyen Van A</option>
          <option value="2">Dinh Van B</option>
          <option value="3">Le Van C</option>
        </select>
      ),
    },
  ],
  11: [
    {
      title: "Order number",
      key: "orderNumber",
      dataIndex: "orderNumber",
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      render: (status) => ORDER_STATUS[status],
    },
    {
      title: "User",
      key: "userName",
      dataIndex: "userName",
    },
    {
      title: "Phone",
      key: "userPhone",
      dataIndex: "userPhone",
    },
    {
      title: "Payment Method",
      key: "paymentName",
      dataIndex: "paymentName",
    },
    {
      title: "Price",
      key: "orderPrice",
      dataIndex: "orderPrice",
    },
    {
      title: "Shipping Fees",
      key: "shippingFee",
      dataIndex: "shippingFee",
    },
    {
      title: "Shipper",
      key: "shipperId",
      dataIndex: "shipperId",
    },
  ],
  13: [
    {
      title: "Order number",
      key: "orderNumber",
      dataIndex: "orderNumber",
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      render: (status) => ORDER_STATUS[status],
    },
    {
      title: "User",
      key: "userName",
      dataIndex: "userName",
    },
    {
      title: "Phone",
      key: "userPhone",
      dataIndex: "userPhone",
    },
    {
      title: "Payment Method",
      key: "paymentName",
      dataIndex: "paymentName",
    },
    {
      title: "Price",
      key: "orderPrice",
      dataIndex: "orderPrice",
    },
    {
      title: "Shipping Fees",
      key: "shippingFee",
      dataIndex: "shippingFee",
    },
  ],
  17: [
    {
      title: "Order number",
      key: "orderNumber",
      dataIndex: "orderNumber",
    },
    {
      title: "Status",
      key: "status",
      dataIndex: "status",
      render: (status) => ORDER_STATUS[status],
    },
    {
      title: "User",
      key: "userName",
      dataIndex: "userName",
    },
    {
      title: "Phone",
      key: "userPhone",
      dataIndex: "userPhone",
    },
    {
      title: "Payment Method",
      key: "paymentName",
      dataIndex: "paymentName",
    },
    {
      title: "Price",
      key: "orderPrice",
      dataIndex: "orderPrice",
    },
    {
      title: "Shipping Fees",
      key: "shippingFee",
      dataIndex: "shippingFee",
    },
  ],
};

export const ORDER_DETAIL_COLUMNS = [
  {
    title: "Image",
    key: "image",
    dataIndex: "image",
    render: (image, record) => (
      <img src={image} alt={record.name} className="h-4 w-auto" />
    ),
  },
  {
    title: "Name",
    key: "name",
    dataIndex: "name",
  },
  {
    title: "Price",
    key: "price",
    dataIndex: "price",
  },
  {
    title: "Discount",
    key: "discount",
    dataIndex: "discount",
  },
  {
    title: "Qty",
    key: "quantity",
    dataIndex: "quantity",
  },
  {
    title: "Total",
    render: (_, record) => record.price * record.quantity,
  },
];
