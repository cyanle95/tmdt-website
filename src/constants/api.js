const API = {
  IMAGE: {
    delete(id) {
      return `deleteImage/${id}`;
    },
  },
  AUTH: {
    LOGIN: "accounts/login",
  },
  PRODUCT: {
    GET_LIST: "sale/products?sort=0",
    GET_LIST_BY_CATEGORY: "sale/products?sort=0?categoryId=",
    POST: "sale/products",
  },
  BANNER: {
    GET_LIST: "sale/banners",
  },
  CATEGORY: {
    GET_LIST: "sale/categories",
    GET_LIST_TOPICS: `sale/`,
  },
  TOPIC: {
    getList(id) {
      return `sale/${id}/topics`;
    },
  },

};

export default API;
