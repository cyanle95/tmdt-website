import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Carousel } from "react-responsive-carousel";
import { getListBanner, getProducts } from "../../store/shopping/actions";
import "./style.css";
import Products from "./products";
const bannersMock = [
  {
    url:
      "https://prestashop.templatemela.com/PRSADD11/PRS257/modules/tm_imageslider/views/img/sample-2.jpg",
  },
  {
    url:
      "https://prestashop.templatemela.com/PRSADD11/PRS257/modules/tm_imageslider/views/img/sample-1.jpg",
  },
];
const Home = () => {
  const dispatch = useDispatch();
  const { products, banners, isLoading } = useSelector(
    (state) => state.shoppingManagement
  );
  useEffect(() => {
    dispatch(getProducts());
    dispatch(getListBanner());
  }, []);
  console.log(products);
  return (
    <div className="w-full">
      <div className="w-full">
        <Carousel autoPlay infiniteLoop interval={2000} showThumbs={false}>
          {bannersMock.map((item, index) => {
            return (
              <div key={index}>
                <img src={item.url} />
              </div>
            );
          })}
        </Carousel>
      </div>
      <div className="max-w-1200 m-auto py-5">
        <Products
          title={"Sản phẩm bán chạy"}
          data={products.filter(
            (item) => item.quantity < 300 && item.quantity > 50
          )}
        />
        <Products
          title={"Sản phẩm mới"}
          data={products.filter((item) => item.quantity > 300)}
        />
      </div>
    </div>
  );
};

export default Home;
