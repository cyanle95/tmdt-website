import React, { useState } from "react";
import Product from "./product";
const Products = (props) => {
  return (
    <div className="w-full">
      <div className="p-3 md:p-4 text-xl font-medium">{props.title}</div>
      <div className="max-w-1200 flex justify-start flex-wrap">
        {props.data?.map((item, key) => {
          return (
            <div
              key={key}
              className="w-1/2 sm:w-1/3 lg:w-1/4 2xl:w-1/5 px-3 md:px-4"
            >
              <Product data={item} />
            </div>
          );
        })}
      </div>
      <div className="p-3 text-xl font-medium flex justify-center items-center">
        <button
          type="button"
          class="inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-gray-800 hover:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800"
        >
          Xem thêm
        </button>
      </div>
    </div>
  );
};
export default Products;
