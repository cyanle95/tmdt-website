import React, { useState } from "react";
import Rating from "react-rating";
import Icon from "../../components/Icon";

const Product = (props) => {
  const [isHover, setIsHover] = useState(false);
  return (
    <div
      className="m-auto w-full mb-5 border border-gray-300 shadow rounded-md cursor-pointer"
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
    >
      <div className="w-full px-10 my-3 rounded-t-md relative overflow-hidden">
        <div
          className={`w-10 h-36 absolute left-1 transition-all duration-500 z-10 ${
            isHover ? "opacity-100 top-5" : "opacity-0 top-2"
          }`}
        >
          <div className="w-10 h-10 flex justify-center items-center rounded-full hover:bg-black group cursor-pointer">
            <Icon
              name="cart"
              className="h-4 w-4 fill-current text-black group-hover:text-white"
            />
          </div>
          <div className="w-10 h-10 flex justify-center items-center rounded-full hover:bg-black group cursor-pointer">
            <Icon
              name="heart"
              className="h-4 w-4 fill-current text-black group-hover:text-white"
            />
          </div>
          <div className="w-10 h-10 flex justify-center items-center rounded-full hover:bg-black group cursor-pointer">
            <Icon
              name="eye-open"
              className="h-4 w-4 fill-current text-black group-hover:text-white"
            />
          </div>
          <div className="w-10 h-10 flex justify-center items-center rounded-full hover:bg-black group cursor-pointer">
            <Icon
              name="resize"
              className="h-4 w-4 fill-current text-black group-hover:text-white"
            />
          </div>
        </div>
        <img
          src={props.data?.image}
          className="transform transition duration-500 hover:scale-105 object-contain w-44 h-44"
        />
      </div>
      <div className="w-full p-2 text-center font-medium">
        <div className="xl:py-2">
          <Rating
            emptySymbol={
              <Icon
                name="star"
                className="h-3 w-3 lg:h-4 lg:w-4 fill-current text-gray-200 "
              />
            }
            fullSymbol={
              <Icon
                name="star"
                className="h-3 w-3 lg:h-4 lg:w-4 fill-current text-yellow-300"
              />
            }
            placeholderSymbol={
              <Icon
                name="star"
                className="h-3 w-3 lg:h-4 lg:w-4 fill-current text-yellow-300"
              />
            }
            initialRating={1}
            readonly
          />
        </div>
        <p className="m-0 p-1">Voluptatem accusantium</p>
        <p className="m-0 p-1">2,000,000</p>
      </div>
    </div>
  );
};
export default Product;
