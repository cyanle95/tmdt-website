import React, { useState } from "react";
import { ReactComponent as ArrowRight } from "../../../../assets/icons/arrowRight.svg";
import { ReactComponent as Heart } from "../../../../assets/icons/heart.svg";

import RateStart from "../RateStart";

const ProductItem = (props) => {
  const { product } = props;
  const [isHover, setIsHover] = useState(false);

  return (
    <div className="flex flex-col xl:flex-row group px-0 xl:px-4 ml-5 xl:ml-0 w-full h-57 md:items-center border-2 border-gray-300 sm:max-w-xs md:max-w-sm xl:max-w-full rounded-xl mb-4">
      <img
        src={product.image}
        className="transition pt-5 self-center xl:pt-0 duration-300 ease-in-out xl:pl-20  w-40 h-40 object-contain transform group-hover:-translate-y-1 group-hover:scale-110"
      />
      <div className="flex flex-col h-full p-3 xl:p-3 xl:ml-20">
        <p className="text-2xl mb-0 xl:mb-4 font-semibold">Bình ga</p>
        <p className="mb-3 hidden xl:block">
          Space for a small product description
        </p>
        <RateStart />
        <div className="hidden xl:flex flex-row w-full mt-2">
          <p className="w-1/3">Fresheness</p>
          <p className="w-2/3 text-left ml-5 ">New(Extra fresh)</p>
        </div>
        <div className="hidden xl:flex  flex-row w-full ">
          <p className="w-1/3">Farm</p>
          <p className="w-2/3 text-left ml-5">Pham Anh Dung</p>
        </div>
        <div className="hidden xl:flex flex-row w-full ">
          <p className="w-1/3">Delivery</p>
          <p className="w-2/3 text-left ml-5">Europe</p>
        </div>
        <div className="hidden xl:flex flex-row w-full ">
          <p className="w-1/3">Stock</p>
          <p className="w-2/3 text-left ml-5">320 pcs</p>
        </div>
      </div>
      <div className="flex flex-col md:flex-row xl:flex-col h-full p-5 pt-0 xl:pt-5 xl:ml-20">
        <div className="flex flex-col mr-4 xl:mr-0">
          <div className="flex flex-row items-center md:flex-col">
            <p className="text-2xl mb-0 xl:mb-4 font-semibold">36,99 USD</p>
            <p className="mb-0 xl:mb-5 ml-4 md:ml-0 line-through">50,01 USD</p>
          </div>
          <div className="flex flex-row items-center md:flex-col mb-3">
            <p className="mb-0 text-gray-400 font-semibold">Free Shipping</p>
            <span className=" text-gray-400 ml-4 md:ml-0 ">
              Delivery in 1 day
            </span>
          </div>
        </div>

        <div className="flex flex-col">
          <button className="flex flex-row items-center mb-3 shadow-md rounded-md  text-white bg-green-500 p-3 hover:bg-green-600">
            <span class="mr-1 text-l font-semibold">Product Detail</span>
            <ArrowRight className="w-3 h-3 stroke-current text-white stroke-2" />
          </button>
          <button className="flex flex-row items-center shadow-md rounded-md  text-black bg-gray-100 p-3 ">
            <span class="mr-1 text-l font-semibold">Add to wish list</span>
            <Heart className="w-3 h-3 stroke-current text-black stroke-2" />
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProductItem;
