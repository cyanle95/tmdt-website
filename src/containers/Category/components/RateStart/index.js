import React from "react";
import ReactStars from "react-rating-stars-component";

export default function RateStart() {
  return (
    <ReactStars
      classNames="mb-0 xl:mb-4"
      count={5}
      // onChange={ratingChanged}
      size={18}
      edit={false}
      value={4}
      activeColor="#FDBC15"
    />
  );
}
