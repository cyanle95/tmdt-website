import React from "react";

export default function Footer() {
  return (
    <div className="flex flex-row p-14 justify-center items-center text-green-500">
      <div className="flex flex-col w-1/4 space-y-3 ">
        <h2 className="font-semibold text-lg text-black">Get in touch</h2>
        <span>About us</span>
        <span>Carrers</span>
        <span>Press Releases</span>
        <span>Blog</span>
      </div>
      <div className="flex flex-col w-1/4 space-y-3">
        <h2 className="font-semibold text-lg text-black">Connections</h2>
        <span>Facebook</span>
        <span>Twitter</span>
        <span>Instagram</span>
        <span>LinkedIn</span>
      </div>
      <div className="flex flex-col w-1/4 space-y-3">
        <h2 className="font-semibold text-lg text-black">Earnings</h2>
        <span>Become an Affiliate</span>
        <span>Advertise your product</span>
        <span>Sell on Market</span>
      </div>
      <div className="flex flex-col w-1/4 space-y-3">
        <h2 className="font-semibold text-lg text-black">Account</h2>
        <span>Your Account</span>
        <span>Returns Center</span>
        <span>!00% purchase protection</span>
        <span>Chat with us</span>
      </div>
    </div>
  );
}
