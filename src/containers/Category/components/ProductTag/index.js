import React from "react";

export default function ProductTag() {
  const data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
  return (
    <div className="flex flex-col p-14">
      <h1 className="font-semibold text-lg text-black">Product Tag</h1>
      <div className="flex flex-row flex-wrap py-5">
        {data.map((item) => (
          <button className="bg-gray-200 text-black rounded-full py-1 px-3  mx-5 mb-3">
            Beans
          </button>
        ))}
      </div>
    </div>
  );
}
