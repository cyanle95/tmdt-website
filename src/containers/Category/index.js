import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { withLocalize } from "react-localize-redux";
import "react-input-range/lib/css/index.css";
import { useParams } from "react-router-dom";
import "./styles.css";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import Product from "../Shopping/components/Product";
import ProductItem from "./components/ProductItem";
import {
  getProductsByCategory,
  getListBanner,
} from "../../store/shopping/actions";
import shoppingTranslation from "./translation/index.json";
import { Checkbox } from "antd";
import ReactStars from "react-rating-stars-component";
import InputRange from "react-input-range";
import Button from "../../components/Button";
import Pagging from "./components/Pagging";
import Footer from "./components/Footer";
import ProductTag from "./components/ProductTag";
const Category = (props) => {
  const { translate } = props;
  const { id } = useParams();
  const [value, setValue] = useState({ min: 2, max: 10 });
  const [type, setType] = useState(1);
  const dispatch = useDispatch();
  const { productsByCategory, banners } = useSelector(
    (state) => state.shoppingManagement
  );
  console.log("id======:", id);

  useEffect(() => {
    props.addTranslation(shoppingTranslation);
    dispatch(getListBanner());
  }, []);
  useEffect(() => {
    console.log("kkkkk");
    if (id) {
      dispatch(getProductsByCategory(id));
    }
  }, [id]);
  return (
    <div>
      <div className="container mx-auto max-w-screen-xl">
        <div className="flex flex-row justify-center">
          <div className="hidden xl:block w-72 pr-8">
            <div>
              <div className="text-base font-medium">Category menu</div>
              <div className="flex flex-row items-center">
                <Checkbox />
                <div className="text-sm category-item underline ml-3">
                  Bakery
                </div>
              </div>
              <div className="flex flex-row items-center">
                <Checkbox />
                <div className="text-sm category-item underline ml-3">
                  Fruit and vegetables
                </div>
              </div>
              <div className="flex flex-row items-center">
                <Checkbox />
                <div className="text-sm category-item underline ml-3">
                  Meat and fish
                </div>
              </div>
              <div className="flex flex-row items-center">
                <Checkbox />
                <div className="text-sm category-item underline ml-3">
                  Kitchen
                </div>
              </div>
            </div>
            <div className="mt-6">
              <div className="text-base font-medium">Rating</div>
              <ReactStars
                count={5}
                // onChange={ratingChanged}
                size={16}
                value={5}
                edit={false}
                activeColor="#FDBC15"
              />
              <ReactStars
                count={5}
                // onChange={ratingChanged}
                size={16}
                edit={false}
                value={4}
                activeColor="#FDBC15"
              />
              <ReactStars
                count={5}
                // onChange={ratingChanged}
                size={16}
                value={3}
                edit={false}
                activeColor="#FDBC15"
              />
              <ReactStars
                count={5}
                // onChange={ratingChanged}
                size={16}
                value={2}
                edit={false}
                activeColor="#FDBC15"
              />
              <ReactStars
                count={5}
                // onChange={ratingChanged}
                edit={false}
                size={16}
                value={1}
                activeColor="#FDBC15"
              />
            </div>
            <div className="w-70 rate">
              <div className="text-base font-medium">Price</div>
              <div style={{ marginTop: "20px" }}>
                <InputRange
                  maxValue={20}
                  minValue={0}
                  value={value}
                  onChange={(value) => setValue(value)}
                />
              </div>
              <div className="w-68 flex flex-row md:justify-between mt-4">
                <div className="">
                  <div>Min</div>
                  <div className="inputRange">{value.min}</div>
                </div>
                <div className="">
                  <div>Max</div>
                  <div className="inputRange">{value.max}</div>
                </div>
              </div>
            </div>
            <div className="filterBtn">
              <Button title="Apply" className="applyBtn" />
              <Button title="Reset" className="resetBtn" />
            </div>
          </div>
          <div className="xl:right">
            <div className="xl:block flex flex-row flex-wrap justify-center">
              {type === 1 &&
                productsByCategory.map((item, index) => {
                  return <ProductItem product={item} />;
                })}
            </div>
          </div>
        </div>
        <Pagging />
      </div>
      <Footer />
      <ProductTag />
    </div>
  );
};

export default withLocalize(Category);
