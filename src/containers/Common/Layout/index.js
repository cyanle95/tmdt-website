import React from "react";
import { connect } from "react-redux";
import TopBar from "../../../components/TopBar";
import Footer from "../../../components/Footer";
import Menu from "../../../components/Menu";

const Layout = (props) => {
  return (
    <div className="w-full flex flex-col bg-white overflow-auto">
      <TopBar
        pageTitle={props.pageTitle}
        notifications={props.notifications}
        user={props.user}
      />
      <Menu />
      <div className="flex h-full w-full overflow-hidden relative">
        <main
          className="flex-1 h-full relative focus:outline-none"
          tabIndex="0"
        >
          {props.children}
        </main>
      </div>
      <Footer />
    </div>
  );
};

const mapStateToProps = (state) => ({
  notifications: state.ui.notifications,
  user: state.ui.user,
  pageTitle: state.ui.pageTitle,
  menu: state.ui.menu,
  isLoading: state.ui.isLoading,
});

export default connect(mapStateToProps, null)(Layout);
