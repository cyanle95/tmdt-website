module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: ["./src/**/*.html", "./src/**/*.js"],
  theme: {
    extend: {
      screens: {
        "2xl": "1920px",
      },
    },
  },
  variants: {
    backgroundColor: ["responsive", "hover", "focus", "active"],
    scale: ["responsive", "hover", "focus", "active", "group-hover"],
    display: ["responsive", "hover", "focus", "active", "group-hover"],
    fontSize: ["responsive", "hover", "focus"],
  },
  plugins: [require("@tailwindcss/ui"), require("@tailwindcss/custom-forms")],
};
